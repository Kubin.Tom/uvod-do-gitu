﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringManipulationConsoleApp.CustomExtensions;

namespace StringManipulationConsoleAppUnitTests
{
    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        [DataRow("Hello World", "helloWorld")]
        [DataRow("test", "test")]
        [DataRow("Bus    Went   mad", "busWentMad")]
        [DataRow("apple sleep", "appleSleep")]
        [DataRow("hello-world", "helloWorld")]
        [DataRow("hello_world", "helloWorld")]
        [DataRow("HelloWorld", "helloWorld")]
        public void ToCamelCase_WhenCalled_ChangeStringToCamelCase(string actual, string expected)
        {
            var actualInCamelCaseForm = actual.ToCamelCase();

            Assert.AreEqual(expected, actualInCamelCaseForm);
        }
    }
}
