﻿using StringManipulationConsoleApp.Classes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringManipulationConsoleApp.CustomExtensions
{
    public static class StringExtensions
    {
        public static string ToTitleCase(this string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        public static string ToCamelCase(this string text)
        {
            var textType = text.ToTextType();

            var textArray = StringSplitter.Split(text, textType);

            textArray[0] = textArray[0].ToLower();
            for (int i = 1; i < textArray.Length; i++) textArray[i] = textArray[i].ToTitleCase();

            return string.Join("", textArray);
        }

        /// <summary>
        /// Return text type, where type is from StringType enum
        /// kebab-case, snake_case, ..
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static StringType ToTextType(this string text)
        {
            if ((text.Split(StringTypesDelimiters.RegularCaseDelimiter, '\t')).Length > 1) return StringType.Regular;
            if ((text.Split(StringTypesDelimiters.KebabCaseDelimiter)).Length > 1) return StringType.KebabCase;
            if ((text.Split(StringTypesDelimiters.SnakeCaseDelimiter)).Length > 1) return StringType.SnakeCase;

            return StringType.PascalCase;
        }
     
    }
}
